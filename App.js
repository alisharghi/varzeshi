import React ,{useState} from 'react';
import { View , Text ,StatusBar , TouchableOpacity ,I18nManager } from 'react-native';
import styles from './src/styles'

const App = () => {
  StatusBar.setBarStyle('light-content')
  StatusBar.setBackgroundColor('#ee0000')
  I18nManager.allowRTL(false)
  I18nManager.forceRTL(false)
  const [goal,setGoal] = useState([0,0])
  const [chance,setchance] = useState([0,0])
  const [pass,setPass] = useState([0,0,0,0])
  const [shoot,setShoot] = useState([0,0,0,0])
  const [card,setCard] = useState([0,0,0,0])
  const [corner,setCorner] = useState([0,0])
  const [foul,setFoul] = useState([0,0])
  const [time,setTime] = useState([0,0])
  const [intRef,setIntRef] = useState([undefined,undefined])

  
  const timing = (teamNum)=>{
    switch(teamNum){
      case 0:{
        if(intRef[0]===undefined){
          setIntRef([setInterval(()=>{
            setTime(prev=>{
              return [prev[0]+1,time[1]]
            })
          },1000),clearInterval(intRef[1])])
        }
        else{
          clearInterval(intRef[0])
          setIntRef([undefined,undefined])
        }
        break
      }
      case 1:{
        if(intRef[1]===undefined){
          setIntRef([clearInterval(intRef[0]),setInterval(()=>{
            setTime(prev=>{
              return[time[0],prev[1]+1]
              })
          },1000)])
        }
        else{
          clearInterval(intRef[1])
          setIntRef([undefined,undefined])
        }
        break
      }
    }
  }
  const increament = (row,index,step)=>{
    let prev
    switch(row){
      case 0 : {
        prev = [...goal]
        prev[index] +=step
        setGoal(prev)
        break
      }
      case 1 : {
        prev = [...chance]
        prev[index] +=step
        setchance(prev)
        break
      }
      case 2 : {
        prev = [...pass]
        prev[index] +=step
        setPass(prev)
        break
      }
      case 3 : {
        prev = [...shoot]
        prev[index] +=step
        setShoot(prev)
        break
      }
      case 4 : {
        prev = [...card]
        prev[index] +=step
        setCard(prev)
        break
      }
      case 5 : {
        prev = [...corner]
        prev[index] +=step
        setCorner(prev)
        break
      }
      case 6 : {
        prev = [...foul]
        prev[index] +=step
        setFoul(prev)
        break
      }
    }
  }
  return (
    <View style={styles.headerView}>
    <Text style={styles.headerTitle}>ثبت اطلاعات بازی</Text>
    <View style={styles.parentView}>
    <View style={styles.teamNameView}>
      <Text style={styles.teamNameText}>تیم میزبان</Text>
      <Text style={styles.teamNameText}>تیم مهمان</Text>
    </View>
    <View style={styles.rowSeprator}/>

    <View style={styles.rowParent}>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(0,0,1)} onLongPress={()=>increament(0,0,-1)} style={styles.positiveTouchableView}><Text style={styles.touchableText}>{goal[0]}</Text></TouchableOpacity>
      </View>
      <View style={styles.teamResultSeprator}/>
      <Text style={styles.attrNameText}>گل</Text>
      <View style={styles.teamResultSeprator}/>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(0,1,1)} onLongPress={()=>increament(0,1,-1)} style={styles.positiveTouchableView}><Text style={styles.touchableText}>{goal[1]}</Text></TouchableOpacity>
      </View>
    </View>
    <View style={styles.rowSeprator}/>

    <View style={styles.rowParent}>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(1,0,1)} onLongPress={()=>increament(1,0,-1)} style={styles.touchable}><Text style={styles.touchableText}>{chance[0]}</Text></TouchableOpacity>
      </View>
      <View style={styles.teamResultSeprator}/>
      <Text style={styles.attrNameText}>شانس گل</Text>
      <View style={styles.teamResultSeprator}/>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(1,1,1)} onLongPress={()=>increament(1,1,-1)} style={styles.touchable}><Text style={styles.touchableText}>{chance[1]}</Text></TouchableOpacity>
      </View>
    </View>
    <View style={styles.rowSeprator}/>


    <View style={styles.rowParent}>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(2,0,1)} onLongPress={()=>increament(2,0,-1)} style={styles.positiveTouchableView}><Text style={styles.touchableText}>{pass[0]===0?'صحیح':pass[0]}</Text></TouchableOpacity>
      <TouchableOpacity onPress={()=>increament(2,1,1)} onLongPress={()=>increament(2,1,-1)} style={styles.negativeTouchable}><Text style={styles.touchableText}>{pass[1]===0?'غلط':pass[1]}</Text></TouchableOpacity>
      </View>
      <View style={styles.teamResultSeprator}/>
      <Text style={styles.attrNameText}>پاس</Text>
      <View style={styles.teamResultSeprator}/>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(2,2,1)} onLongPress={()=>increament(2,2,-1)} style={styles.positiveTouchableView}><Text style={styles.touchableText}>{pass[2]===0?'صحیح':pass[2]}</Text></TouchableOpacity>
      <TouchableOpacity onPress={()=>increament(2,3,1)} onLongPress={()=>increament(2,3,-1)} style={styles.negativeTouchable}><Text style={styles.touchableText}>{pass[3]===0?'غلط':pass[3]}</Text></TouchableOpacity>
      </View>
    </View>
    <View style={styles.rowSeprator}/>

    <View style={styles.rowParent}>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(3,0,1)} onLongPress={()=>increament(3,0,-1)} style={styles.blueToucahble}><Text style={styles.touchableText}>{shoot[0]===0?'داخل':'('+shoot[0]+')'}</Text></TouchableOpacity>
      <TouchableOpacity onPress={()=>increament(3,1,1)} onLongPress={()=>increament(3,1,-1)} style={styles.touchable}><Text style={styles.touchableText}>{shoot[1]===0?'خارج':shoot[1]}</Text></TouchableOpacity>
      </View>
      <View style={styles.teamResultSeprator}/>
      <Text style={styles.attrNameText}>شوت</Text>
      <View style={styles.teamResultSeprator}/>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(3,2,1)} onLongPress={()=>increament(3,2,-1)} style={[styles.blueToucahble]}><Text style={styles.touchableText}>{shoot[2]===0?'داخل':'('+shoot[2]+')'}</Text></TouchableOpacity>
      <TouchableOpacity onPress={()=>increament(3,3,1)} onLongPress={()=>increament(3,3,-1)} style={styles.touchable}><Text style={styles.touchableText}>{shoot[3]===0?'خارج':shoot[3]}</Text></TouchableOpacity>
      </View>
    </View>
    <View style={styles.rowSeprator}/>


    <View style={styles.rowParent}>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(5,0,1)} onLongPress={()=>increament(5,0,-1)} style={styles.touchable}><Text style={styles.touchableText}>{corner[0]}</Text></TouchableOpacity>
      </View>
      <View style={styles.teamResultSeprator}/>
      <Text style={styles.attrNameText}>کرنر</Text>
      <View style={styles.teamResultSeprator}/>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(5,1,1)} onLongPress={()=>increament(5,1,-1)} style={styles.touchable}><Text style={styles.touchableText}>{corner[1]}</Text></TouchableOpacity>
      </View>
    </View>
    <View style={styles.rowSeprator}/>

    <View style={styles.rowParent}>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(6,0,1)} onLongPress={()=>increament(6,0,-1)} style={styles.touchable}><Text style={styles.touchableText}>{foul[0]}</Text></TouchableOpacity>
      </View>
      <View style={styles.teamResultSeprator}/>
      <Text style={styles.attrNameText}>خطا</Text>
      <View style={styles.teamResultSeprator}/>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(6,1,1)} onLongPress={()=>increament(6,1,-1)} style={styles.touchable}><Text style={styles.touchableText}>{foul[1]}</Text></TouchableOpacity>
      </View>
    </View>
    <View style={styles.rowSeprator}/>

    <View style={styles.rowParent}>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(4,0,1)} onLongPress={()=>increament(4,0,-1)} style={styles.yellowToucahble}><Text style={styles.touchableText}>{card[0]===0?'زرد':card[0]}</Text></TouchableOpacity>
      <TouchableOpacity onPress={()=>increament(4,1,1)} onLongPress={()=>increament(4,1,-1)} style={styles.negativeTouchable}><Text style={styles.touchableText}>{card[1]===0?'قرمز':card[1]}</Text></TouchableOpacity>
      </View>
      <View style={styles.teamResultSeprator}/>
      <Text style={styles.attrNameText}>کارت</Text>
      <View style={styles.teamResultSeprator}/>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>increament(4,2,1)} onLongPress={()=>increament(4,2,-1)} style={styles.yellowToucahble}><Text style={styles.touchableText}>{card[2]===0?'زرد':card[2]}</Text></TouchableOpacity>
      <TouchableOpacity onPress={()=>increament(4,3,1)} onLongPress={()=>increament(4,3,-1)} style={styles.negativeTouchable}><Text style={styles.touchableText}>{card[3]===0?'قرمز':card[3]}</Text></TouchableOpacity>
      </View>
    </View>
    <View style={styles.rowSeprator}/>


    <View style={styles.rowParent}>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>timing(0)} style={intRef[0]===undefined?styles.touchable:styles.blueToucahble}><Text style={styles.touchableText}>{time[0]||time[1]!==0?(time[0]/(time[0]+time[1])*100).toFixed(0)+'%':'0'}</Text></TouchableOpacity>
      </View>
      <View style={styles.teamResultSeprator}/>
      <Text style={styles.attrNameText}>درصد مالکیت</Text>
      <View style={styles.teamResultSeprator}/>
      <View style={styles.rowSubmitView}>
      <TouchableOpacity onPress={()=>timing(1)} style={intRef[1]===undefined?styles.touchable:styles.blueToucahble}><Text style={styles.touchableText}>{time[0]||time[1]!==0?(time[1]/(time[0]+time[1])*100).toFixed(0)+'%':'0'}</Text></TouchableOpacity>
      </View>
    </View>
    <View style={styles.rowSeprator}/>


    </View>
    </View>
  );
};

export default App;